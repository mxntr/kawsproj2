﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kawsproj2.Startup))]
namespace Kawsproj2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
